# Analyse comparative de E.coli et de V.cholerae


## Auteures

Etudiantes :

* Leroy 		Cassandre
* Lachiheb 	Sarah

Sorbonne Université (UPMC) 2017

## Préambule

Le but de ce projet est d'analyser de manière comparative le génome de deux bactéries : un organisme déjà bien connu E.coli (souche, qui n'est pas pathogène) et une bactérie pathogène des Entérocoques le V.cholerae.

Le but est d'analyser les propriétés générales du génome, de détecter les différentes régions de composition homogéne, de pouvoir trouver les éléments génétiques qui lui confère sa toxicité, et de comprendre comment cette toxicité a été acquise.

Ce sujet d'étude est organisé en trois parties successives pour pouvoir analyser les propriétés des génomes :

* Partie A : propriété globales des génomes et détection des hétérogénéités dans la séquence.

* Partie B : Analyse comparative, détection des îlots de pathogénicité, analyse des propriétés, annotation des génes correspoondants.

* Partie C : Analyse de l'usage des codons et lien avec les propriétés des génes et les îlots de pathogénicité.

## Rendus

Fichiers  |
------------- | 
[Présentation](https://github.com/LeroyProjects/Bio_informatique/blob/master/Pr%C3%A9sentation.pdf) |
[Rapport](https://github.com/LeroyProjects/Bio_informatique/blob/master/Rapport.pdf) |
[Code sources](https://gitlab.com/ProjetLachiheb/analyse_genome/tree/master/Code_sources) |
