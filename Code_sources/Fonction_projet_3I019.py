# -*- coding: utf-8 -*-
import sys
import os

###############################################################
#############Préliminaires, propriétés de base#################
###############################################################

def listeFichierTab(fichier) :
    #sorti = os.popen("cut -f3,4,5,8 {} | tail -n +2 ".format(fichier), "r")
    sorti = os.popen("cut -f3,4,5,8 {}".format(fichier), "r")
    texte = sorti.read()
    sorti.close()
    liste = list(texte.replace('\t', ' ').split("\n"))
    i = 0
    while (i < len(liste)):
       liste[i]=liste[i].split() # ligne.split() renvoie une liste de toutes les chaines\
       i += 1
    liste.remove(list())
    return liste

def stringFichierGenome(fichier) :
    """Utiliser par la fonction listeFichierTab(fichier), extrait le cont\
enu du fichier.genome"""
    #sorti = os.popen("tail -n +2 {}".format(fichier), "r")
    sorti = open(fichier, "r")
    texte = sorti.read()
    texte = texte.replace("\n", "")
    sorti.close()
    return texte

def listeCodons (chaine) :
    """Return une liste de codons pour une sequence passée en parametre ici chaine.
       Si la longueur de la séquence n'est pas multiple de 3, on ne tiendra pas
       compte des 1 ou 2 nucléotide restant à la fin"""
    list = []
    for i in range(0, len(chaine), 3) :
        if ((i+3) > len(chaine)) == False :
            list.append(chaine[i:i+3])
    return list

# Question 2.d/2.e :
def extractionGeneFasta(fichierTab, fichierGenome, f1, f2) :
    """Extrait les gènes du fichier.genome à partir des coordonnées du fichier.tab en version FASTA"""
    tab = listeFichierTab(fichierTab)
    genome = stringFichierGenome(fichierGenome)
    # Ouvrir un fichier :
    mon_fichier1 = open(f1, "w")
    mon_fichier2 = open(f2, "w")
    i = 0;
    while (i < len(tab)) :
        erreur = False
        gene = genome[int(tab[i][0])-1: int(tab[i][1])] #-1 -3
        if (tab[i][2] == '-') :
            liste = list(gene)
            liste.reverse()
            for j, elem in enumerate(liste) :
                if (elem == 'A') :
                    liste[j] = 'T'
                elif (elem == 'T') :
                    liste[j] = 'A'
                elif elem == 'G' :
                    liste[j] = 'C'
                elif elem == 'C' :
                    liste[j] = 'G'
                else :
                    erreur = True
            gene = "".join(liste)
        else :
            liste = list(gene)
            for elem in liste :
                if elem not in ['A','T','G','C'] :
                    erreur = True
                    break
            gene = "".join(liste)
         #Verification :
        if erreur == False :
            codons = listeCodons (gene)
            if (codons[0] in ['ATG', 'GTG', 'TTG']) :
                taille = len(codons)
                codons = codons[0: taille-1]
                bool = False
                for c in codons :
                    if c in ["TAA","TAG","TGA"] :
                        bool = True
                        break
                if bool == True :
                    mon_fichier2.write(">{}\n".format(tab[i][3]))
                    mon_fichier2.write("{}\n".format(gene))
                else :
                    mon_fichier1.write(">{}\n".format(tab[i][3]))
                    mon_fichier1.write("{}\n".format(gene))
            else :
                mon_fichier2.write(">{}\n".format(tab[i][3]))
                mon_fichier2.write("{}\n".format(gene))
        i += 1
    mon_fichier1.close()
    mon_fichier2.close()


# Variable globale
codeGenetique = {"TTT":"F","TTC":"F","TTA":"L","TTG":"L","TCT":"S","TCC":"S",
                                "TCA":"S","TCG":"S","TAT":"Y","TAC":"Y","TAA":"*","TAG":"*",
                                "TGT":"C","TGC":"C","TGA":"*","TGG":"W","CTT":"L","CTC":"L",
                                "CTA":"L","CTG":"L","CCT":"P","CCC":"P","CCA":"P","CCG":"P",
                                "CAT":"H","CAC":"H","CAA":"Q","CAG":"Q","CGT":"R","CGC":"R",
                                "CGA":"R","CGG":"R","ATT":"I","ATC":"I","ATA":"I","ATG":"M",
                                "ACT":"T","ACC":"T","ACA":"T","ACG":"T","AAT":"N","AAC":"N",
                                "AAA":"K","AAG":"K","AGT":"S","AGC":"S","AGA":"R","AGG":"R",
                                "GTT":"V","GTC":"V","GTA":"V","GTG":"V","GCT":"A","GCC":"A",
                                "GCA":"A","GCG":"A","GAT":"D","GAC":"D","GAA":"E","GAG":"E",
                                "GGT":"G","GGC":"G","GGA":"G","GGG":"G"}

def traductionGeneEnProteine (gene) :
    """ traduit un gene en proteine et là renvoie sous forme de liste """
    sequenceCodon = listeCodons(gene)
    traductionProteine = []
    for codon in sequenceCodon :
        traductionProteine.append(codeGenetique[codon])
    return traductionProteine

def traduction_gene_codant (fichier_extract) :
    sortie1 = os.popen("grep -v '>' {}".format(fichier_extract), "r")
    sortie2 = os.popen("grep '>' {}".format(fichier_extract), "r")
    gene = sortie1.read().split()
    chevron = sortie2.read().split()
    #fichier = open("II_proteine_codant", "w")
    fichier = open("I_II_proteine_codant", "w")
    for c,g in zip(chevron, gene) :
        fichier.write("{}\n".format(c))
        g = traductionGeneEnProteine(g)
        g = "".join(g)
        fichier.write("{}\n".format(g))
    sortie1.close()
    sortie2.close()
    fichier.close()
#########################################################
######################BLAST##############################
#########################################################
#Question 2.f :
def pourcentageGC (genes_fasta, fichier_tab) :
    str_id = recupere_identifiant_genes_fasta (genes_fasta)
    sortie = os.popen("grep -v '>' {}".format(genes_fasta), "r")
    contenue = sortie.read()
    contenue = contenue.split()
    dico = {}
    for id,elem in zip(str_id, contenue) :
        dico[id] = ((elem.count('C') + elem.count('G'))/ len(elem))*100
    sortie = open(fichier_tab, "r")
    fichier = open("E.coli_codant_GC.tab", "w")
    for contenue in sortie.readlines():
        contenue = contenue.split() 
        id = contenue[7]
        if id in str_id : 
        #contenue.append(str(round(dico[id],2)))
            chaine = "\t".join(contenue)
            fichier.write(str(round(dico[id],2)) + "\t" + chaine + "\n")
    fichier.close()
    sortie.close()

def recupere_identifiant_genes_fasta (genes_fasta) :
    sortie = os.popen("grep '>' {} | tr -d '>'".format(genes_fasta), "r")
    liste = []
    for contenue in sortie.readlines():
        contenue = contenue.split()
        str_id = "".join(contenue)
        liste.append(str_id)
    print(liste)
    return liste
#######################################################################
def taille_gene (fichier_tab) :
    sortie = os.popen("tail -n +2 {} | cut -f3,4,8".format(fichier_tab), "r")
    res = []
    for contenue in sortie.readlines():
        res.append(contenue.split())
    dico = {}
    for el in res : 
        som = int(el[1]) - int(el[0])
        dico[el[2]] = som
    sortie.close()
    return dico

# Question 3.c :
def ajout_colonne_blast(fichier_tab, fichier_blast) :
    dico = taille_gene(fichier_tab)
    #print(1)
    sortie = open(fichier_blast, "r")
    #print(2)
    res = []
    for contenue in sortie.readlines():
        contenue = contenue.split()
        taille = dico[contenue[0]]
        resultat = int((((int(contenue[7])-int(contenue[6]))*3) / int(taille)) *100)
        if (resultat < 80) :
            continue
        contenue.append(str(resultat))
        res.append(contenue)
    fichier = open("correpondance.blast", "w")
    for elem in res :
        chaine = "\t".join(elem)
        fichier.write(chaine + "\n")
    fichier.close()
    sortie.close()
#########################################################################
# Question 3.d :
def nombre_alignement_significatif (fichier_tab, fichier_correspondance) :
    dico_id = compte_gene_fichier_correspondance (fichier_correspondance)
    sortie = open(fichier_tab, "r")
    fichier = open("Vibrio_cholerae_GC_nbAlign.tab", "w")
    for contenue in sortie.readlines():
        tmp = contenue.split()
        id = tmp[8]
        fichier.write(str(dico_id.count(id)) + "\t" + str(contenue) + "\n")
    sortie.close()
    fichier.close()

def compte_gene_fichier_correspondance (fichier_correspondance) :
    sortie = os.popen("cut -f1 {}".format(fichier_correspondance), "r")
    liste = []
    # Creation liste total et sans doublons d'identifiant :
    for contenue in sortie.readlines():
        contenue = contenue.split()
        str_id = "".join(contenue)
        liste.append(str_id)
    return liste
###########################################################################
#Assignation des catégories fonctionnelles pour les gènes codants avec COG#
###########################################################################
# Question 5 :
def COG_annotation (fichier_annotation) :
    sortie = os.popen("tail -n +2 {} | cut -f1,9,17".format(fichier_annotation), "r")
    dico = {}
    for contenue in sortie.readlines():
        contenue = contenue.split()
        dico[contenue[1]] = [contenue[0], contenue[2]]
    sortie.close()
    return dico

# Question 6 :
def correspondance_bis (fichier_annotation, fichier_correspondance) :
    dico = COG_annotation (fichier_annotation)
    sortie = open(fichier_correspondance, "r")
    res = []
    for contenue in sortie.readlines():
        contenue = contenue.split()
        contenue.append(dico[contenue[1]][0]) # COG_ID
        contenue.append(dico[contenue[1]][1]) # COG_FUNC
        res.append(contenue)
    fichier = open("correpondance_COGid_COGfunction.blast", "w")
    for elem in res :
        chaine = "\t".join(elem)
        fichier.write(chaine + "\n")
    fichier.close()

######################################################################################
#########Visualisation et navigation dans un génome, analyse de la composition########
######################################################################################
# I : NC_002505.1
# II : NC_002506.1

#Question 9.a :
def tableau_igv (genome_chrom1_50bp, genome_chrom2_50bp):
    chrom1 = "NC_002505.1"
    chrom2 = "NC_002506.1"
    # Chromosome 1 :
    sortie1 = open(genome_chrom1_50bp, "r")
    sortie2 = open(genome_chrom2_50bp, "r")
    fichier1 = open("I_Vibrio_cholerae_genome_50bp_GC","w")
    for bloc in sortie1.readlines() :
        bloc = "".join(bloc.split())
        # Calcul de pourcentage GC,A et T par bloc :
        perGC = round(((bloc.count('C') + bloc.count('G'))/ len(bloc))*100,2)
        perA = round((bloc.count('A')/ len(bloc))*100,2)
        perT = round((bloc.count('T')/ len(bloc))*100,2)
        fichier1.write(str(perGC) + " " + str(perA) + " " + str(perT) + "\n")
    # Chromosome 2 :
    sortie1.close()
    fichier1.close()
    fichier2 = open("II_Vibrio_cholerae_genome_50bp_GC","w")
    for bloc in sortie2.readlines() :
        bloc = "".join(bloc.split())
        # Calcul de pourcentage GC,A et T par bloc :
        perGC = round(((bloc.count('C') + bloc.count('G'))/ len(bloc))*100,2)
        perA = round((bloc.count('A')/ len(bloc))*100,2)
        perT = round((bloc.count('T')/ len(bloc))*100,2)
        fichier2.write(str(perGC) + " " + str(perA) + " " + str(perT) + "\n")
    sortie2.close()
    fichier2.close()
    tab = open("tableau.igv", "w")
    # Construction du tableau :
    fichier1 = open("I_Vibrio_cholerae_genome_50bp_GC","r")
    fichier2 = open("II_Vibrio_cholerae_genome_50bp_GC","r")
    deb = 0
    for b in fichier1.readlines() :
        b = b.split()
        perGC, perA, perT = b[0], b[1], b[2]
        tab.write(chrom1 + "\t" + str(deb) + "\t" + str(deb+50) + "\t" + "TEST" + "\t" + perGC + "\t" + perA + "\t" + perT + "\n")
        deb += 50
    deb = 0
    for b in fichier2.readlines() :
        b = b.split()
        perGC, perA, perT = b[0], b[1],b[2]
        tab.write(chrom2 + "\t" + str(deb) + "\t" + str(deb+50) + "\t" + "TEST" +  "\t" + perGC + "\t" + perA + "\t" + perT + "\n")
        deb += 50
    fichier1.close()
    fichier2.close()
    tab.close()

def decoupage_genome_50bp (fichier_genome):
    # Recupere le fichier et supprime les sauts de ligne :
    sortie = open(fichier_genome, "r")
    contenu = sortie.read()
    contenu = contenu.replace("\n", "")
    # Creer un fichier contenant le genome en 50bp :
    sortie2 = open("II_Vibrio_cholerae_genome_50bp.fasta", "w")
    for i,n in enumerate(contenu):
        i+=1
        sortie2.write(n)
        if (i % 50) == 0 :
            sortie2.write("\n")
    sortie.close()
    sortie2.close()
######################################################################################

#Question 9.b :
def GC_gene_annote (fichier_COGid_COGfunction, fichier_Vibrio_cholerae_GC_tab) :
    sortie1 = os.popen("cat {} | grep -Ev 'NA|X' | cut -f1 | uniq".format(fichier_COGid_COGfunction), "r")
    contenu = sortie1.read()
    sortie1.close
    # %GC - nom_chrom - id
    sortie2 = os.popen("cut -f1,9 {}".format(fichier_Vibrio_cholerae_GC_tab), "r")
    # Identifiant des genes annote :
    liste_id_gene = contenu.split()
    # Fichier contenant %GC des genes annotes :
    fichier = open("gene_annote_GC", "w")
    # dictionnaire : identidiant => %GC :
    for line in sortie2.readlines() :
        line = line.split()
        if line[1] in liste_id_gene :
            fichier.write(line[1] + "\t" + line[0] + "\n")
    fichier.close()
    sortie2.close()

def extractionGeneFasta1(fichierTab, fichierGenome, f1, f2) :
    """Extrait les gènes du fichier.genome à partir des coordonnées du fichier.tab en version FASTA"""
    tab = listeFichierTab(fichierTab)
    genome = stringFichierGenome(fichierGenome)
    # Ouvrir un fichier :
    mon_fichier2 = open(f2, "w")
    i = 0;
    while (i < len(tab)) :
        erreur = False
        gene = genome[int(tab[i][0])-1: int(tab[i][1])] #-1 -3
        if (tab[i][2] == '-') :
            liste = list(gene)
            liste.reverse()
            for j, elem in enumerate(liste) :
                if (elem == 'A') :
                    liste[j] = 'T'
                elif (elem == 'T') :
                    liste[j] = 'A'
                elif elem == 'G' :
                    liste[j] = 'C'
                elif elem == 'C' :
                    liste[j] = 'G'
                else :
                    erreur = True
            gene = "".join(liste)
        mon_fichier2.write(">{}\n".format(tab[i][3]))
        mon_fichier2.write("{}\n".format(gene))
        i += 1
    mon_fichier2.close()

def tableau_igv_annotation(fichier_annotation ,fichier1, fichier2) :
    chrom1 = "NC_002505.1"
    chrom2 = "NC_002506.1"
    # lire gene_annotation_GC :
    dico = {}
    fichier = open(fichier_annotation, "r")
    for line in fichier.readlines() :
        line = line.split()
        dico[line[0]] = line[1]
    fichier.close()
    fichier = open("tableau_annotation.igv", "w")
    sortie1 = open(fichier1, "r")
    # Chromosome1 :
    for line in sortie1.readlines() :
        line = line.split()
        if line[2] in dico :
            fichier.write(chrom1 + "\t" + line[0] + "\t" + line[1] + "\t" + line[2] + "\t" + dico[line[2]] + "\n")
    sortie1.close()
    sortie2 = open(fichier2, "r")
    # Chromosome2 :
    for line in sortie2.readlines() :
        line = line.split()
        if line[2] in dico :
            fichier.write(chrom2 + "\t" + line[0] + "\t" + line[1] + "\t" + line[2] + "\t" + dico[line[2]] + "\n")
    sortie2.close()
    fichier.close()

#######################################################################################
#####################                  MAIN                ############################
#######################################################################################
""" Une partie des appels de fonctions utilisées durant le Projet :""" 

#tableau_igv_annotation(sys.argv[1] , sys.argv[2], sys.argv[3])
#GC_gene_annote (sys.argv[1], sys.argv[2])
#GC_gene_annote (sys.argv[1])
#tableau_igv(sys.argv[1], sys.argv[2])
#decoupage_genome_50bp (sys.argv[1])
#pourcentageGC (sys.argv[1], sys.argv[2])
#recupere_identifiant_genes_fasta (sys.argv[1])
#nombre_alignement_significatif (sys.argv[1], sys.argv[2])
#compte_gene_fichier_correspondance (sys.argv[1])
#correspondance_bis (sys.argv[1], sys.argv[2])
#COG_annotation (sys.argv[1])
#ajout_colonne_blast(sys.argv[1], sys.argv[2])
#taille_gene (sys.argv[1])
#traduction_gene_codant(sys.argv[1])
#pourcentageGC (sys.argv[1])
#extractionGeneFasta(sys.argv[1], sys.argv[2], "E.coli_gene_codant", "E.coli_gene_non_codant")
#extractionGeneFasta1(sys.argv[1], sys.argv[2], "E1", "E2")
