import csv
import matplotlib.pyplot as plt

bins = []
y = []
y2=[]

inf = 1000
sup = 2000

####################################################################################################
#############Fichier pour faire tous les histogrammes et graphe du projet entier : #################
####################################################################################################

with open('../../NBAL', 'r') as csvfile : 
    plots = csv.reader(csvfile)
    for row in plots:
        y.append(float(row[0]))

#with open('TEST2', 'r') as csvfile :
#    plots = csv.reader(csvfile)
#    for row in plots:
#        y2.append(float(row[0]))


#with open('100resV', 'r') as csvfile :
#    plots = csv.reader(csvfile)
#    for row in plots:
#        y2.append(float(row[0]))

bins = [bins for bins in range(len(y))]                                                                 

#res2 = []
#bins = [bins for bins in range(sup - inf)]
#for i in range(sup - inf):
#    res2.append(y[i+inf])

#i = 1
#for el in z :
#    el = 1000 * i
#    i += 1
#    bins.append(el)

#res = []
#for i in range(sup - inf):
#    res.append(y[i+inf])

#res2 = []
#for i in range(sup - inf):
#    res2.append(y2[i+inf])

res = []
deb = []
"""
deb = [0]*100
res=[0]*100

for i in range(100) :
    deb[i] = i

for i in range(100) :
    res[i] = y.count(i)
"""
#for el in range(101):
#    deb.append(el)
#    res.append(y.count(el))

#g= 0
#for el in res :
#    g += el

#print(g)
#print(len(res))

#plt.bar(deb, res) # alpha = 0.8)
plt.bar(bins, y, color = 'b')
#plt.bar(bins, y2, color = 'r', alpha = 0.4, label = 'Pourcentage des codons pour les gènes pathogènes.')
#plt.plot(bins, y, color = 'k', label = 'Genes non alignés de V.Cholerae',linewidth = 1)#  color = 'k')
#plt.axhline(55, color='r', linewidth=4, alpha = 0.5)
#plt.axhline(35, color='g', linewidth=4, alpha = 0.5)
#plt.axhline(45.9, color='k', linewidth=2, alpha = 0.5)

#plt.axvline(51, color='r', linewidth=2)


#plt.fill_between(bins, res, 60,where=(res > 60), facecolor='g', alpha=0.5)
#plt.spines['left'].set_color('c')

#plt.plot(50, 0, color = 'r')
#plt.hist(res, 1000, normed=1, facecolor='b', alpha=0.5)

#plt.xlabel('Blocs de 1Kbp')
#plt.ylabel(u'Pourcentage en GC')
#plt.axis([0, 1000, 0, 70])
#plt.grid(True)
#plt.show()

plt.xlabel('Nombre de fois que le gènes est aligné')
#plt.ylabel('Nombre d'alignement de ')
labels = ['0', '20', '30', '40', '50', '60', '70', '80', '100']
#plt.xticks(res, labels)
#plt.margins(0.2)
#plt.subplots_adjust(bottom=0.15)
plt.title('Répartition des alignement de V.Cholerae contre E.Coli')
plt.legend()
plt.show()
