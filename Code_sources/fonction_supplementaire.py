import os
import sys

freq_par_aa = {"AAA":0, "AAC":0, "AAG":0, "AAT":0, "ACA":0, "ACC":0, "ACG":0, "ACT":0,
"AGA":0, "AGC":0, "AGG":0, "AGT":0, "ATA":0, "ATC":0, "ATG":0, "ATT":0,
"CAA":0, "CAC":0, "CAG":0, "CAT":0, "CCA":0, "CCC":0, "CCG":0, "CCT":0,
"CGA":0, "CGC":0, "CGG":0, "CGT":0, "CTA":0, "CTC":0, "CTG":0, "CTT":0,
"GAA":0, "GAC":0, "GAG":0, "GAT":0, "GCA":0, "GCC":0, "GCG":0, "GCT":0,
"GGA":0, "GGC":0, "GGG":0, "GGT":0, "GTA":0, "GTC":0, "GTG":0, "GTT":0,
"TAA":0, "TAC":0, "TAG":0, "TAT":0, "TCA":0, "TCC":0, "TCG":0, "TCT":0,
"TGA":0, "TGC":0, "TGG":0, "TGT":0, "TTA":0, "TTC":0, "TTG":0, "TTT":0, }

############################################################################################
#################### Fonction supplémentaire pour calculer la fréquence des codons #########
############################################################################################
def frequence_codont_gene_codant_par_gene(fichier_genes_codant) :
    fichier = open(fichier_genes_codant)
    liste_gene = []
    # Recupere les genes en format str:
    for gene in fichier.readlines() :
        gene = gene.replace("\n","")
        liste_gene.append(gene)
    # Remplir le dictionnaire de frequence par acide aminé :
    for gene in liste_gene :
        # Re-initialiser freq_par_aa à 0 :
        for key in freq_par_aa.keys() :
            freq_par_aa[key] = 0
        # Remplissage :
        for codon in listeCodons(gene) :
            freq_par_aa[codon] += 1
        # Pourcentage :
        som = 0
        for value in freq_par_aa.values():
            som += value
        for key in freq_par_aa.keys():
            freq_par_aa[key] = (freq_par_aa[key] /som) * 100 
        # Affichage :
        print(">")
        for key, value in freq_par_aa.items() :
            #freq_par_aa[key] = round(value,2)
            print("{}\t{}".format(key, round(value,2)))
    #som = 0
    #for value in freq_par_aa.values():
    #    som += value
    #print(som)
    fichier.close()

def frequence_codont_gene_codant_total(fichier_genes_codant) :
    fichier = open(fichier_genes_codant)
    sortie = fichier.read()
    sortie = sortie.replace("\n", "")
    liste = listeCodons(sortie)
    for codon in liste :
        freq_par_aa[codon] += 1
    som = 0
    for value in freq_par_aa.values():
            som += value
    for key in freq_par_aa.keys():
        freq_par_aa[key] = (freq_par_aa[key] /som) * 100
    for key, value in freq_par_aa.items() :
        #freq_par_aa[key] = round(value,2)
        print("{}\t{}".format(key, round(value,2)))
    #som = 0
    #for value in freq_par_aa.values():
    #    som += value
    #print(som)
    fichier.close()

def listeCodons (chaine) :
    """Return une liste de codons pour une sequence passée en parametre ici chaine.
       Si la longueur de la séquence n'est pas multiple de 3, on ne tiendra pas
       compte des 1 ou 2 nucléotide restant à la fin"""
    list = []
    for i in range(0, len(chaine), 3) :
        if ((i+3) > len(chaine)) == False :
            list.append(chaine[i:i+3])
    return list

def lecture_par_gene (fichier) :
    """Dictionnaire par gene"""
    fichier = open(fichier)
    res = []
    for i,elem in enumerate(fichier.readlines()) :
        liste = elem.split()
        if liste[0] == ">" :
            if i > 0 :
                res.append(dico)
            dico = {}
            continue
        else :
            dico[liste[0]] = float(liste[1])
    return res
            
def lecture_entier (fichier) :
    """Dictionnaire dans une genome"""
    fichier = open(fichier)
    dico = {}
    for elem in fichier.readlines() :
        liste = elem.split()
        dico[liste[0]] = float(liste[1])
    return dico
 
def recuperer_liste_codons () :
    liste = []
    for key in freq_par_aa.keys():
        liste.append(key)
    print(liste)

#######################################################################################
#####################                  MAIN                ############################
#######################################################################################

""" Une partie des appels de fonctions utilisées durant le Projet """

#print(ecuperer_liste_codons())
#print(lecture_par_gene (sys.argv[1]))   
#print(lecture_entier (sys.argv[1]))
#frequence_codont_gene_codant_par_gene(sys.argv[1])
#frequence_codont_gene_codant_total(sys.argv[1])
