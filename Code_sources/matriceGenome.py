import os

###############################################################
####################Matrice De Confusion#######################
###############################################################


###############################################################
####################liste positions débuts et fins des gènes###

def listesDFGenome(fichier):
    commande = "grep -v '>' " + fichier + " | cut -f3,4 | tail -n +2"
    l = os.popen(commande)
    contenu = l.read()
    line = contenu.split("\n")
    final= []

    for e in line:
        res = e.split()
        final.append(res)
    final.pop()

    cpt = 1
    while cpt < len(final):
        if final[cpt-1][0] == final[cpt][0]:
            if final[cpt][1] > final[cpt+1][1]:
                del final[cpt+1]
            else:
                del final[cpt]
        cpt += 1
    # final: liste de liste => 2 éléments de GENE = deb, fin. 
    # On cherche faire une liste de début et une de fin : 
    positions_debut = []
    positions_fin = []

    for e in final : 
        positions_debut.append(int(e[0]))
        positions_fin.append(int(e[1]))

    positions_debut.append(0)
    
    return positions_debut, positions_fin


###############################################################
####################liste positions débuts et fins des ORF#####

def listesDFOrf(fichier):
    commande = "grep -v '>' " + fichier
    l = os.popen(commande)
    contenu = l.read()
    line = contenu.split("\n")
    final= []

    for e in line:
        res = e.split()
        final.append(res)
    final.pop()

    cpt = 1
    while cpt < len(final):
        if final[cpt-1][0] == final[cpt][0]:
            if final[cpt][1] > final[cpt+1][1]:
                del final[cpt+1]
            else:
                del final[cpt]
        cpt += 1
    # final: liste de liste => 2 éléments de GENE = deb, fin.                                                                                                                                                                          
    # On cherche faire une liste de début et une de fin :                                                                                                                                                                              
    positions_debut = []
    positions_fin = []

    for e in final :
        positions_debut.append(int(e[0]))
        positions_fin.append(int(e[1]))

    positions_debut.append(0)

    return positions_debut, positions_fin

###############################################################
#################### ecrit_intervalle   #######################

def liste01(positions_debut, positions_fin):
    commande = "grep -v '>' Escherichia.coli.genome"
    l = os.popen(commande)
    contenu = l.read()
    
    lg = len(contenu)
    res = []

    l = 0
    while l < lg:
        res.append(0)
        l += 1

    i = 1
    cpt = 0
    while i <= lg:
        if i == positions_debut[cpt] :
            res[i] = 1
            nb = 0
            val = i
            while nb < (positions_fin[cpt] - val):
                res[i+nb] = 1
                nb += 1
            cpt += 1
        i += 1
    return res

###################################################################################
####################fonction matrice de confusion / compare_intervalle#############

def matrice(liste1, liste2):
    l = [0]*2
    for i in range(len(l)) :
        l[i] = [0]*2
    for g, o in zip(liste1, liste2) :
        l[int(g)][int(o)] += 1
    return l

# Sensibilite :
def sens(l):
    res = (l[1][1] / (l[1][1] + l[1][0]))
    return res

# Specificite :
def spe(l):
    res = (l[0][0] / (l[0][0] + l[0][1]))
    return res

# Valeur predictive :
def vp(l):
    res = (l[1][1] / (l[1][1] + l[0][1]))
    return res


######################## MAIN  ########################### 

att1, att2 = listesDFGenome("Escherichia.coli.tab")
listeGenes = liste01(att1, att2)

att11, att12 = listesDFOrf("orf.tab")
listeORF = liste01(att11, att12)

l = matrice(listeGenes, listeORF)

print("Sensibilité : " + str(sens(l)))
print("Spécificité : " + str(spe(l)))
print("Valeur prédictive : " + str(vp(l)))
